package com.ftl.search;

import com.alibaba.fastjson.JSON;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.IndexOperations;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;

import java.io.IOException;
import java.util.*;

@SpringBootTest
class SearchApiApplicationTests {

    @Autowired
    private RestHighLevelClient client;

    @Autowired
    private ElasticsearchRestTemplate restTemplate;

    @Test
    void contextLoads() {
    }

    @Test
    void testSelect() throws IOException {
        String id = "LP121907297FR";
        SearchRequest request = new SearchRequest("oms_oms_shipment");
        SearchSourceBuilder builder = new SearchSourceBuilder();
        builder.query(QueryBuilders.matchPhraseQuery("trackingnumber", id));
        builder.from(0);
        builder.size(3);
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        org.elasticsearch.search.SearchHit[] hits = response.getHits().getHits();
        List<Map<String, Object>> result = new ArrayList<>();
        for (org.elasticsearch.search.SearchHit hit : hits) {
            result.add(hit.getSourceAsMap());
        }
        System.out.println(result);
    }

    @Test
    void createAndDeleteIndex() {
        String indexName = "wwf_test";
        IndexOperations indexOperations = restTemplate.indexOps(IndexCoordinates.of(indexName));
        if (!indexOperations.exists()) {
            indexOperations.create();
            indexOperations.refresh();
            System.out.println("索引" + indexName + "创建成功");
        } else {
            System.out.println("索引" + indexName + "已存在");
        }
        if (indexOperations.exists()) {
            indexOperations.delete();
            System.out.println("索引" + indexName + "删除成功");
        }
    }

    class TestUser {

        private Long id;
        private String name;
        private String robby;

        public TestUser(Long id, String name, String robby) {
            this.id = id;
            this.name = name;
            this.robby = robby;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRobby() {
            return robby;
        }

        public void setRobby(String robby) {
            this.robby = robby;
        }
    }

    @Test
    void createIndexAndPutData() {
        String indexName = "www";
        IndexOperations indexOperations = restTemplate.indexOps(IndexCoordinates.of(indexName));
        if (!indexOperations.exists()) {
            indexOperations.create();
            indexOperations.refresh();
        }
        Object ir = restTemplate.save(new TestUser(23L,"wwf2", "taxi23"), IndexCoordinates.of(indexName));
        System.out.println("数据保存成功, 返回结果：" + JSON.toJSONString(ir));
        String dr = restTemplate.delete("22", IndexCoordinates.of(indexName));
        System.out.println("数据删除，返回结果：" + dr);
    }

}

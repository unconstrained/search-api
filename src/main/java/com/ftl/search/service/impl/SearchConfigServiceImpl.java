package com.ftl.search.service.impl;

import com.ftl.search.model.QueryDefinition;
import com.ftl.search.model.RestConfigDefinition;
import com.ftl.search.model.RestQueryDefinition;
import com.ftl.search.model.SearchDefinition;
import com.ftl.search.repository.SearchDefinitionWriter;
import com.ftl.search.service.SearchConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;
import java.util.Set;

@Service
@Slf4j
public class SearchConfigServiceImpl implements SearchConfigService {

    @Resource
    private SearchDefinitionWriter searchDefinitionWriter;

    @Override
    public void save(SearchDefinition searchDefinition) {
        log.info("更新搜索配置：{}", searchDefinition);
        searchDefinitionWriter.save(searchDefinition);
    }

    @Override
    public Map<String, QueryDefinition> getSearches() {
        return searchDefinitionWriter.getSearches();
    }

    @Override
    public Map<String, Set<String>> getTemplates() {
        return searchDefinitionWriter.getTemplates();
    }

    @Override
    public Map<String, RestConfigDefinition> getRestMapping() {
        return searchDefinitionWriter.getRestMapping();
    }

    @Override
    public Map<String, RestQueryDefinition> getRestTemplates() {
        return searchDefinitionWriter.getRestTemplates();
    }
}

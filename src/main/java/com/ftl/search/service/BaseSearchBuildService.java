package com.ftl.search.service;

import com.ftl.search.model.FieldDefinition;
import com.ftl.search.model.QueryDefinition;
import com.ftl.search.model.dto.BaseSearchDTO;
import org.elasticsearch.action.search.SearchRequest;
import java.util.List;
import java.util.Map;

public interface BaseSearchBuildService {

    /**
     * 发起查询
     * @param keywords
     * {
     *     must: Map<String, String>,
     *     should: Map<String, String>
     * }
     * @param authClients
     * @return List<T>
     */
    List<Map<String, Object>> queryForEntity(String indexName, Map<String, List<FieldDefinition>> keywords, List<String> authClients, int size);

    Map<String, List<Map<String, Object>>> queryForEntities(List<BaseSearchDTO> searches, List<String> authClients);

    SearchRequest buildQuery(String indexName, Map<String, List<FieldDefinition>> keywords, List<String> authClients, int size);

    Map<String, List<FieldDefinition>> buildKeyword(QueryDefinition definition, String value, List<String> values);
}

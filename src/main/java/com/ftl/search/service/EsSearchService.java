package com.ftl.search.service;

import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;

import java.util.List;
import java.util.Map;

public interface EsSearchService {

    List search(String keyword, String value, Class<?> clazz);

    Map<String, List> multiSearch(List<NativeSearchQuery> builders, List<Class<?>> classes, List<String> keys);

    NativeSearchQuery buildQuery(String keyword, String value);

    boolean checkIndexExists(String indexName);

    void createIndex(String indexName);

    void deleteIndex(String indexName);

    /**
     * obj中如果存在id则会更新，没有则会新增
     * @param indexName
     * @param obj
     */
    void save(String indexName, Object obj);

    void updateByDocumentId(String indexName, String documentId, Object obj);

    /**
     * 更新文档部分字段
     * @param indexName
     * @param documentId
     * @param obj
     */
    void updatePartialFields(String indexName, String documentId, Object obj);

    void deleteByDocumentId(String indexName, String documentId);

    void deleteByObject(String indexName, Object obj);

    Object selectByDocumentId(String indexName, String documentId);
}

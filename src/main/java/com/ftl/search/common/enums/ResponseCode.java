package com.ftl.search.common.enums;

public enum ResponseCode {
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,

    /**
     * 异常
     */
    ERROR,

    /**
     * 无数据
     */
    NOT_FOUND,

    /**
     * 不存在
     */
    NOT_EXISTS
}

package com.ftl.search.common.enums;

public enum ConditionType {
    EQUAL,
    LIKE,
    IN
}

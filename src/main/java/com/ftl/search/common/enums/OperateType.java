package com.ftl.search.common.enums;

public enum OperateType {

    /**
     * 创建索引
     */
    CREATE_INDEX,

    /**
     * 创建数据
     */
    CREATE_DATA,

    /**
     * 更新数据
     */
    UPDATE,

    /**
     * 删除索引
     */
    DELETE_INDEX,

    /**
     * 删除数据
     */
    DELETE_DATA,

    /**
     * 部分更新
     */
    PARTIAL_UPDATE
}

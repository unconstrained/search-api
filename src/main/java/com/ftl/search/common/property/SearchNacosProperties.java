package com.ftl.search.common.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("search.config")
@Component
@Data
public class SearchNacosProperties {

    private String dataId;

    private String group;

    private String namespace;
}

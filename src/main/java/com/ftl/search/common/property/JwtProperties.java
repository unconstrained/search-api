package com.ftl.search.common.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("search.jwt")
public class JwtProperties {

    private String secret;

    private String authKey;
}

package com.ftl.search.common.context;

import com.ftl.search.model.User;

public class LoginUserContext {
    private final static ThreadLocal<User> USER_THREAD_LOCAL = new ThreadLocal<>();

    public LoginUserContext() {
    }

    public static void setLoginUser(User user) {
        USER_THREAD_LOCAL.set(user);
    }

    public static User getLoginUser() {
        return USER_THREAD_LOCAL.get();
    }

    public static void remove() {
        USER_THREAD_LOCAL.remove();
    }
}

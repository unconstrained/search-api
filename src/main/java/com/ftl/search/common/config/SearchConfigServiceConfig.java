package com.ftl.search.common.config;

import com.alibaba.cloud.nacos.NacosConfigProperties;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import com.ftl.search.common.property.SearchNacosProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.Properties;

@Configuration
public class SearchConfigServiceConfig {

    @Resource
    private SearchNacosProperties searchNacosProperties;

    @Resource
    private NacosConfigProperties nacosConfigProperties;

    @Bean
    public ConfigService configService() throws NacosException {
        Properties properties = new Properties();
        properties.setProperty(PropertyKeyConst.SERVER_ADDR, nacosConfigProperties.getServerAddr());
        properties.setProperty(PropertyKeyConst.NAMESPACE, searchNacosProperties.getNamespace());
        properties.setProperty(PropertyKeyConst.USERNAME, nacosConfigProperties.getUsername());
        properties.setProperty(PropertyKeyConst.PASSWORD, nacosConfigProperties.getPassword());
        return NacosFactory.createConfigService(properties);
    }
}

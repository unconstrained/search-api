package com.ftl.search.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JoinDefinition {

    /**
     * es查询模板
     */
    private String template;

    /**
     * 从上级结果中取值
     */
    private String sourceKey;

    /**
     * 关联表中的字段 foreignKey == sourceKey
     */
    private String foreignKey;

    /**
     * 如果type == "STRING", 获取targetKey属性值，如果targetKey没值则返回查询结果
     */
    private String targetKey;

    /**
     * 目标字段
     */
    private String prettyKey;

    /**
     * 返回类型，STRING,MAP,LIST
     */
    private String type = "LIST";

    /**
     * 默认关联数据
     */
    private int size = 3;
}

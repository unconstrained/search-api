package com.ftl.search.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestQueryDefinition {

    private String restName;

    private String key;

    private Map<String, String> prettyFields;
}

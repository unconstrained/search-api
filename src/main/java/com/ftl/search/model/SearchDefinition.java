package com.ftl.search.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchDefinition {

    private Map<String, QueryDefinition> mapping;

    private Map<String, Set<String>> template;

    private Map<String, RestConfigDefinition> restMapping;

    private Map<String, RestQueryDefinition> restTemplate;
}

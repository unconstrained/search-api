package com.ftl.search.model;

import com.ftl.search.common.enums.ConditionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FieldDefinition {

    private String field;

    private ConditionType condition;

    private String value;

    private List<String> values;
}

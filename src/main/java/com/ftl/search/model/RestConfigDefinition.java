package com.ftl.search.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.elasticsearch.rest.RestRequest;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestConfigDefinition {

    // 请求方式
    private RestRequest.Method method;

    // 请求地址
    private String url;

    // 请求头
    private Map<String, String> headers;

    // 主要参数名称
    private String primaryKey;

    // 参数名
    private List<String> paramKey;

    // 请求路径占位符
    private String pathField;

    // LIST, MAP
    private String resultType;

    // 排序字段，目前只支持倒序
    private String sortField;
}

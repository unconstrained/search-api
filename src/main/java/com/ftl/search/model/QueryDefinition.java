package com.ftl.search.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QueryDefinition {

    private String indexName;

    private String key;

    private List<FieldDefinition> must;

    private List<FieldDefinition> should;

    private Map<String, String> prettyFields;

    private List<JoinDefinition> append;
}

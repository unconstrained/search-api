package com.ftl.search.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class SearchParamDTO {

    @NotNull(message = "mode 不能为空")
    private String mode;

    @NotNull(message = "查询入参必须有值")
    private List<String> ids;

    /**
     * 查询条数
     */
    private Integer size;
}

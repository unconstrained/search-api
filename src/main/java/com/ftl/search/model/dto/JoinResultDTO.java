package com.ftl.search.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JoinResultDTO {

    /**
     * 源字段
     */
    private String sourceKey;

    /**
     * 目标字段
     */
    private String prettyKey;

    /**
     * 源数据
     */
    private String sourceValue;

    /**
     * 目标结果
     */
    private Object values;
}

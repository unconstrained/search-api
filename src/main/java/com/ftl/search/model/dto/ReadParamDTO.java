package com.ftl.search.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReadParamDTO {

    @NotNull
    private String id;

    @NotNull
    private String indexName;
}

package com.ftl.search.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RestParamDTO {

    @NotNull
    private List<String> ids;

    @NotNull
    private String mode;

    private Map<String, Object> options;
}

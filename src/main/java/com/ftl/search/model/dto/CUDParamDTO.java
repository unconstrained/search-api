package com.ftl.search.model.dto;

import com.ftl.search.common.enums.OperateType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Arrays;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CUDParamDTO {

    @NotNull(message = "操作类型不能为空")
    private OperateType operate;

    @NotNull(message = "索引名称不能为空")
    private String indexName;

    private Object data;

    private String id;

    public void setIndexName(String indexName) {
        this.indexName = indexName.toLowerCase();
    }

    public void setOperate(String operate) {
        try {
            this.operate = OperateType.valueOf(operate);
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("操作类型必须是" + Arrays.toString(OperateType.values()));
        }
    }
}

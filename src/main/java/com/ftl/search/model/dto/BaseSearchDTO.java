package com.ftl.search.model.dto;

import com.ftl.search.model.FieldDefinition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseSearchDTO {
    private String indexName;
    private Map<String, List<FieldDefinition>> keywords;
    private String key;
}

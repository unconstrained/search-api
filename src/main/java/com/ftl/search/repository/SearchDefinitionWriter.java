package com.ftl.search.repository;

import com.ftl.search.model.QueryDefinition;
import com.ftl.search.model.RestConfigDefinition;
import com.ftl.search.model.RestQueryDefinition;
import com.ftl.search.model.SearchDefinition;

import java.util.Map;
import java.util.Set;

public interface SearchDefinitionWriter {
    void save(SearchDefinition query);

    Map<String, QueryDefinition> getSearches();

    Map<String, Set<String>> getTemplates();

    Map<String, RestConfigDefinition> getRestMapping();

    Map<String, RestQueryDefinition> getRestTemplates();
}

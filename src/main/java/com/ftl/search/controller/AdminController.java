package com.ftl.search.controller;

import com.ftl.search.common.context.LoginUserContext;
import com.ftl.search.common.enums.ResponseCode;
import com.ftl.search.model.User;
import com.ftl.search.model.dto.CUDParamDTO;
import com.ftl.search.model.dto.ReadParamDTO;
import com.ftl.search.model.dto.RestParamDTO;
import com.ftl.search.model.dto.SearchParamDTO;
import com.ftl.search.model.vo.ResponseVO;
import com.ftl.search.service.impl.CommonSearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.repository.query.Param;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController()
@RequestMapping("search")
public class AdminController {

    @Resource
    private CommonSearchService commonSearchService;

    @GetMapping
    public Object search(@Param("id") String id, @Param("mode") String mode) {
        log.info("查询入参：id：{}，mode： {}", id, mode);
        List<String> authClients = new ArrayList<>();
        User user = LoginUserContext.getLoginUser();
        if ("CLIENT".equals(user.getType())) {
            authClients.add(user.getOwner());
        }
        if (StringUtils.hasText(mode)) {
            return commonSearchService.list(id, mode, authClients);
        }
        return commonSearchService.multiSearch(id, authClients, null);
    }

    /**
     * 批量查询
     * @param param
     * @return
     */
    @PostMapping("/customSearch")
    public Object Search(@RequestBody @Validated(SearchParamDTO.class) SearchParamDTO param) {
        List<String> authClients = new ArrayList<>();
        User user = LoginUserContext.getLoginUser();
        if ("CLIENT".equals(user.getType())) {
            authClients.add(user.getOwner());
        }
        return commonSearchService.batchList(param.getIds(), param.getMode(), authClients, param.getSize());
    }

    @GetMapping("byTemplate/{template}")
    public Object searchForTemplate(@PathVariable("template") String template, @Param("id") String id, @Param("mode") String mode) {
        log.info("模板查询入参： template： {}， id：{}， mode：{}", template, id, mode);
        if (!StringUtils.hasText(template)) {
            throw new IllegalArgumentException("模板参数未输入");
        }
        List<String> authClients = new ArrayList<>();
        User user = LoginUserContext.getLoginUser();
        if ("CLIENT".equals(user.getType())) {
            authClients.add(user.getOwner());
        }
        if (StringUtils.hasText(mode)) {
            return commonSearchService.list(id, mode, authClients);
        }
        return commonSearchService.multiSearch(id, authClients, template);
    }

    @PostMapping("traces")
    public Object searchTraces(@Validated @RequestBody RestParamDTO param) {
        Map<String, Object> options = param.getOptions();
        if (options == null) {
            options = new HashMap<>();
        }
        return commonSearchService.listByRest(param.getIds(), param.getMode(), options);
    }

    @PostMapping("modify")
    public ResponseVO modifyWithESByInstruct(@Validated @RequestBody CUDParamDTO dto) {
        return commonSearchService.modifyWithES(dto);
    }

    @PostMapping("findById")
    public ResponseVO findByDocumentId(@Validated @RequestBody ReadParamDTO dto) {
        Object r = commonSearchService.selectByDocumentId(dto.getIndexName(), dto.getId());
        if (ObjectUtils.isEmpty(r)) {
            return new ResponseVO(ResponseCode.NOT_EXISTS.name(), "未查询到数据", r);
        }
        return new ResponseVO(ResponseCode.SUCCESS.name(), "查询到数据", r);
    }
}

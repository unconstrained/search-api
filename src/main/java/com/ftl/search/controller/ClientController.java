package com.ftl.search.controller;

import com.ftl.search.common.context.LoginUserContext;
import com.ftl.search.model.User;
import com.ftl.search.service.impl.CommonSearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.repository.query.Param;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController()
@RequestMapping("client/{clientId}")
public class ClientController {

    @Resource
    private CommonSearchService commonSearchService;

    @GetMapping("search")
    public Object search(@PathVariable String clientId, @Param("id") String id, @Param("mode") String mode) {
        log.info("查询入参：id：{}，mode： {}， client: {}", id, mode, clientId);
        User user = LoginUserContext.getLoginUser();
        if (!StringUtils.hasText(clientId) || !"CLIENT".equals(user.getType()) || !clientId.equals(user.getOwner())) {
            throw new IllegalArgumentException("请求信息违规");
        }
        List<String> authClients = new ArrayList<>();
        authClients.add(user.getOwner());
        if (StringUtils.hasText(mode)) {
            return commonSearchService.list(id, mode, authClients);
        }
        return commonSearchService.multiSearch(id, authClients, null);
    }
}

# search-api

## 运行环境
 - java 11
 - nacos

## Nacos 查询配置
 - mapping  以ES index 为基础的基层查询条件配置
   - 基层查询名称
     - index name
     - 多个查询结果中返回的key
     - prettyFields 字段美化，可以根据原字段名新增别名
     - must 相当于and条件
     - should 相当于or 多个should会组成一个must
       - field 字段名称
       - condition 运算方式，目前只支持 LIKE和EQUAL和IN，IN目前支持批量查询接口使用
       - value 如果是固定入参可以配置
 - template  组合不同的基层查询提供给不同的系统
   - 模板名称 默认模板是DEFAULT
     - 列表的元素为 *基层查询名称*
 - restMapping 以 REST 请求为基础的查询条件配置
   - 基础查询名称，例：trace
     - method 查询方法 POST,GET
     - url 查询地址
     - headers 请求头信息
     - primaryKey param参数名称
     - pathField url中占位符的参数名称
     - resultType 第三方返回结果类型 MAP,LIST
     - sortField 结果排序字段，如果是子层，怎使用".", 例：trackings.timestamp
 - restTemplate 不同的查询配置，自定义组装返回结构
   - 模板名称
     - restName 基础查询名称
     - key 多种请求返回时作为结果的参数值
     - prettyFields 自定义字段别名（Map）
       - ID 增加key未value，值为入参条件之一，
         - 例：ids=[1,2,3]，result=[{"name": "hah", "weight": 8}]，优化后 result=[{"id":1, "name": "hah", "weight": 8}]
       - TYPE 增加type字段存储value，
         - 例：TYPE="sale"，result=[{"name": "hah", "weight": 8}]，优化后 result=[{"name": "hah", "weight": 8, "type": "sale"}]
       - APPEND 增加一个字段value存储子集合
         - 例：APPEND="traces"，result=[{"name": "hah", "weight": 8, "trackings": [{"id"：1, "event": "LIV_DPM"}]}]，优化后 result=[{"name": "hah", "weight": 8, "traces": [{"id"：1, "event": "LIV_DPM"}]}]
       - 其他 相当于字段别名
     - append join查询
       - template 模板名称
       - sourceKey 查询源表字段
       - foreignKey join查询结果的对应字段
       - targetKey join查询结果的目标字段，对应type=STRING
       - prettyKey 返回到结果的美化参数
       - type join查询美化的操作类型
         - STRING 获取targetKey的值
         - MAP 获取join结果的第一个结果
         - LIST 获取join的所有结果
       - size join的查询条数
 ```
 {
    "mapping": {
       "batch": {
            "indexName": "stg_oms_batches",
            "key": "batches",
            "must": [
                {
                    "field": "id",
                    "condition": "LIKE"
                }
            ],
            "prettyFields": {
                "trackingnumber": "trackingNumber"
            },
            "append": [
                {
                    "template": "parcel_batch",
                    "sourceKey": "trackingnumber",
                    "foreignKey": "trackingnumber",
                    "targetKey": "container",
                    "prettyKey": "container",
                    "type": "STRING"
                }
            ]
        }
    },
    "template": {
       "DEFAULT": ["batch", "shipment", "command", "parcel", "destocking", "pallet", "swap_body"]
    },
    "restMapping": {
        "trace": {
            "method": "GET",
            "url": "https://gateway-api-staging.ftlapp.io/api/trace/findTrace/multiple/{code}",
            "headers": {
                "api-key": "xxxx",
                "Authorization": "xxxx"
            },
            "primaryKey": "traceNumbers",
            "pathField": "code",
            "resultType": "MAP",
            "sortField": "timestamp"
        }
    },
    "restTemplate": {
        "BATCH": {
            "restName": "trace",
            "key": "batches",
            "prettyFields": {
                "ID": "number",
                "event": "event",
                "content": "comment",
                "timestamp": "timestamp",
                "TYPE": "MAWB",
                "APPEND": "traces"
            }
        }
    }
 }
 ```

## 接口
#### 通用查询接口
```
    uri /api/search?id=&mode=
    method GET
    header
        Authorization
```
#### 不同模板的调用查询接口
```
    uri /api/search/byTemplate/{template}
    method GET
    header
        Authorization
```
#### 客户特殊查询接口
```
    uri /api/client/{clientId}/search
    method GET
    header
        Authorization
```
#### 批量查询
```
    uri /api/search/customSearch
    method POST
    header 
      Authorization
    body
      mode
      ids
      size
```
#### 批量REST请求
```
    uri /api/search/traces
    method POST
    header 
      Authorization
    body
      ids
      mode
      options
```
#### 使用ES服务的接口，提供增删改操作，查询操作请使用以上接口
```
  uri /api/search/modify
  method POST
  header
    Authorization
  body
    operate
    indexName
    data
    id
  return
    code
    message
    data
```
#### 根据_id精准查询
```
  uri /api/search/findById
  method POST
  header
    Authorization
  body
    id
    indexName
```

## KAFKA
#### 消费并处理数据到ES
- 提供了一个topic：SEARCH_MODIFY_DATA_TOPIC，接收数据并处理
- 数据体必须满足 com.ftl.search.model.dto.CUDParamDTO
- operate: CREATE_INDEX, CREATE_DATA, UPDATE, DELETE_INDEX, DELETE_DATA, PARTIAL_UPDATE

FROM 763365863673.dkr.ecr.eu-west-1.amazonaws.com/base:openjdk11
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} /app.jar
RUN echo "ce7953e8680093c6f72f8753166c20f5" > /datadog/profiling-api-key.txt
ENTRYPOINT java -javaagent:/datadog/dd-java-agent.jar \
-Ddd.trace.analytics.enabled=true \
-Ddd.profiling.enabled=false \
-Ddd.profiling.api-key-file=/datadog/profiling-api-key.txt \
-jar /app.jar
